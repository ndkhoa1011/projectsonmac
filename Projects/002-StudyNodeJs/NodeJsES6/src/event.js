module.exports = {
    studyEvent: () => {
        
        const EventEmitter = require('events');

        class Customer extends EventEmitter{
            constructor(name, gender){
                super();
                this.name = name;
                this.gender = gender;
            }
        };
        
        const mrKhoa = new Customer('khoa', 'male');
        const msNgan = new Customer('Ngan', 'female');
        
        const callBackFunction =  (foods, customer) => {
        for(let index in foods){
            console.log(`${customer.name} call ${foods[index]}`);
        }
        };
        
        mrKhoa.on('eventCallFoods', callBackFunction);
        msNgan.on('eventCallFoods', callBackFunction);
        
        console.log("Khoa doing something");
        
        //Su dung emit() de goi toi ham eventCallFoods
        mrKhoa.emit("eventCallFoods", ["Pizza", "Pepsi"], mrKhoa)
        
        console.log("Ngan doing something");
        console.log("Ngan do something else....");
        
        msNgan.emit("eventCallFoods", ["Milk", "Peach"], msNgan)
    }
}

