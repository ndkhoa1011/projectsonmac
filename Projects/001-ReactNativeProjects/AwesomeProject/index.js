/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import VScrollview from './components/VertialScrollview';
import HScrollView from './components/HorizontalScrollview';
import BasicFlatList from './components/BasicFlatList';
import DeleteItemFlatListExample from './components/DeleteItemFlatList'
import FlatListTutorial from './components/FlatListTutorial'

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => FlatListTutorial);
