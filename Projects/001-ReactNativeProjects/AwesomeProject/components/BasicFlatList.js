import React, { Component } from 'react'
import { View, Text, FlatList, Image } from 'react-native'
import flatListData from '../data/flatListData'

class FlatListItem extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column'
            }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        //backgroundColor: this.props.index % 2 == 0 ? "#90ee90" : "#66cdaa",
                        backgroundColor: '#66cdaa'
                    }}
                >
                    <Image
                        source={{ uri: this.props.item.imageUrl }}
                        style={{ width: 100, height: 100, margin: 10 }}
                    >

                    </Image>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column'
                    }}>
                        <Text
                            style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                padding: 10
                            }}
                        >{this.props.item.name}</Text>
                        <Text style={{
                            fontSize: 14,
                            padding: 10,
                        }}>{this.props.item.foodDescription}</Text>
                    </View>
                </View>

                {/* Add seperated line */}
                <View style={{
                    height: 1,
                    backgroundColor: 'white'
                }}>

                </View>
            </View>
        )
    }
}

export default class BasicFlatListExample extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#ffffff',
                paddingTop: 40
            }}>
                <FlatList
                    data={flatListData}
                    renderItem={
                        ({ item, index }) => {
                            //console.log(`Item: ${JSON.stringify(item)} - Index: ${index}`)
                            return (
                                <FlatListItem item={item} index={index}>

                                </FlatListItem>
                            )
                        }
                    }
                >
                </FlatList>
            </View>
        )
    }
}