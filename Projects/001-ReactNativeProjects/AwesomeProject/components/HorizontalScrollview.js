import React, {Component} from 'react'
import {View, Text, TextInput, 
        StyleSheet, ScrollView,Image, Dimensions} from 'react-native'

export default class HScrollView extends Component {
    constructor(props){
        super(props)
        this.state={
            inputtedText: ''
        }
    }

    render(){
        let screenWidth = Dimensions.get('window').width;
        let screenHeight = Dimensions.get('window').height;

        return(
            <ScrollView
                horizontal={true}
                pagingEnabled={true}
                onMomentumScrollBegin={
                    () => {
                        alert("Begin scrolling")
                    }
                }
                onMomentumScrollEnd={
                    () => {
                        alert("End scrolling")
                    }
                }
            >
                <View style={{
                    flex: 1,
                    backgroundColor:'#663399',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: screenWidth,
                    height: screenHeight,
                }}>
                    <Text style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        color: 'white',
                    }}>
                        Screen 1
                    </Text>
                </View>

                <View style={{
                    flex: 1,
                    backgroundColor:'#b0c4de',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: screenWidth,
                    height: screenHeight,
                }}>
                    <Text style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        color: 'red',
                    }}>
                        Screen 2
                    </Text>
                </View>

                <View style={{
                    flex: 1,
                    backgroundColor:'#90ee90',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: screenWidth,
                    height: screenHeight,
                }}>
                    <Text style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        color: 'red',
                    }}>
                        Screen 3
                    </Text>
                </View>

            </ScrollView>
        )
    }
}
