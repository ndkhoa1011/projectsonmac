import React, { Component } from 'react'
import { View, Text, FlatList, Image, StyleSheet, Alert,
        TouchableHighlight, Platform, YellowBox } from 'react-native'
import flatListData from '../data/flatListData'
import Swipeout from 'react-native-swipeout'
import AddModal from '../components/AddModal'
import EditModal from '../components/EditModal'
import Icon from 'react-native-vector-icons/Ionicons'


class ShowFlatListItem extends Component {
        constructor(props){
                super(props)
                this.state ={
                        activeRowKey: null
                }
        }

        render() {
                const swipeSettings = {
                        autoClose: true,
                        onClose: (secId, seqId, direction) => {
                                if(this.state.activeRowKey != null){
                                        this.setState({
                                                activeRowKey: null
                                        })
                                }
                        },
                        onOpen: (secId, seqId, direction) => {
                                this.setState({
                                        activeRowKey: this.props.item.key
                                })
                        },
                        right: [
                                {
                                        onPress: () =>{
                                                console.log(flatListData[this.props.index])
                                                this.props.parentFlatList.refs.editModal._showModal(flatListData[this.props.index])
                                        },
                                        type: 'primary',
                                        text: 'Edit'
                                },
                                {
                                        onPress: () => {
                                                const deletingRow = this.state.activeRowKey

                                                Alert.alert(
                                                        'Confirm',
                                                        'Are you sure want to delete?',
                                                        [
                                                                {
                                                                        text: 'No',
                                                                        onPress: () => {
                                                                                console.log(this.props.item.key)
                                                                        }
                                                                },
                                                                {
                                                                        text: 'Yes',
                                                                        onPress: () => {
                                                                                flatListData.splice(this.props.index, 1)
                                                                                this.props.parentFlatList._refreshFlatList(deletingRow)
                                                                        }
                                                                }
                                                        ]
                                                )
                                        },
                                        type: 'delete',
                                        text: 'Delete'
                                }
                        ],
                        rowId: this.props.index,
                        secId: 1
                }

                return (
                        <Swipeout {...swipeSettings}>
                                <View style={{
                                        flex: 1,
                                        flexDirection: 'column',
                                        backgroundColor: 'mediumseagreen',

                                }}>
                                        <View style={{
                                                flex: 1,
                                                flexDirection: "row"
                                        }}>
                                                <Image
                                                        source={{ uri: this.props.item.imageUrl }}
                                                        style={{
                                                                width: 100,
                                                                height: 100,
                                                                margin: 10
                                                        }}
                                                >

                                                </Image>
                                                <View style={{
                                                        flex: 1,
                                                        flexDirection: "column",
                                                        margin: 10
                                                }}>
                                                        <Text style={{
                                                                fontSize: 18,
                                                                fontWeight: 'bold',
                                                                color: '#ffffff',
                                                        }}>
                                                                {this.props.item.name}
                                                        </Text>
                                                        <Text>
                                                                {this.props.item.foodDescription}
                                                        </Text>
                                                </View>
                                        </View>

                                        <View style={{ height: 1, backgroundColor: '#ffffff' }}></View>
                                </View>
                        </Swipeout>
                );
        }
}

export default class FlatListTutorial extends Component {
        constructor(props){
                super(props)
                this.state={
                        activedRowKey: null
                }
        }

        _refreshFlatList = (activedKey) =>{
                this.setState(
                        {
                                activedRowKey: activedKey
                        }
                )
                this.refs.myFlatList.scrollToEnd(true)
        }

        _onPressAdd = () =>{
                this.refs.addModal._showModal()
        }

        render() {
                return (
                         <View style={{
                                 flex: 1,
                                 backgroundColor:'#ffffff',
                                 paddingTop: Platform.OS ==='ios' ? 30 : 0
                         }}>
                                <View style={{
                                        flexDirection: "row",
                                        height: 60,
                                        backgroundColor: 'orangered',
                                        justifyContent: 'flex-end',
                                        alignItems: 'center'
                                       
                                }}>
                                        <TouchableHighlight
                                                underlayColor='#ffffff'
                                                onPress={this._onPressAdd}
                                                style={{marginRight: 10}}
                                        >
                                                {/* <Image 
                                                        source={require('../images/add.png')}
                                                        style={{width: 32, height: 32, marginRight: 10}}
                                                /> */}
                                                <Icon name="ios-add" size={40} color='#ffffff'></Icon>
                                        </TouchableHighlight>
                                        
                                </View>
                                <FlatList
                                        ref="myFlatList"
                                        data={flatListData}
                                        renderItem={
                                                ({item, index}) => {
                                                        return(
                                                                <ShowFlatListItem item={item} index={index} parentFlatList={this}></ShowFlatListItem>
                                                        )
                                                }
                                        }
                                        ListEmptyComponent={
                                                ()=>{
                                                        return(
                                                                <View></View>
                                                        )
                                                }
                                        }
                                >
                                </FlatList>

                                <AddModal ref={"addModal"} parentFlatList={this}></AddModal>
                                <EditModal ref={"editModal"} parentFlatList={this}></EditModal>
                         </View>
                );
        }
}