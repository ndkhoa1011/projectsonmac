import React, { Component } from 'react'
import { View, Text, FlatList, Image, Alert } from 'react-native'
import flatListData from '../data/flatListData'
import Swipeout from 'react-native-swipeout'


class FlatListItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeRowKey: null
        }
    }

    render() {
        {/*Config multi prop of compnent. In this case, we config props for Swipeout component*/ }
        const swipeSettings = {
            autoClose: true,
            onClose: (seqId, secId, diretion) => {
                if (this.state.activeRowKey != null) {
                    this.setState(
                        {
                            activeRowKey: null
                        }
                    )
                }
            },
            onOpen: (seqId, secId, diretion) => {
                this.setState(
                    {
                        activeRowKey: this.props.item.key
                    }
                )
            },
            right: [
                {
                    onPress: () => {
                        const deletingRow = this.props.activeRowKey;

                        Alert.alert(
                            'Confirm',
                            'Are you sure want to delete this record?',
                            [
                                {
                                    text: 'No',
                                    onPress: () => console.log('Cancel Pressed'), style: 'cancel'
                                },

                                {
                                    text: 'Yes',
                                    onPress: () => {
                                        flatListData.splice(this.props.index, 1);
                                        //Refresh FlatList ! 
                                        this.props.parentFlatList.refreshFlatList(deletingRow);
                                    }
                                },
                            ],
                            { cancelable: true }
                        )
                    },
                    text: 'Delete',
                    type: 'delete'
                }
            ],
            rowId: this.props.index,
            secId: 1
        };

        return (
            <Swipeout {...swipeSettings}>
                <View style={{
                    flex: 1,
                    flexDirection: 'column'
                }}>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            //backgroundColor: this.props.index % 2 == 0 ? "#90ee90" : "#66cdaa",
                            backgroundColor: '#90ee90'
                        }}
                    >
                        <Image
                            source={{ uri: this.props.item.imageUrl }}
                            style={{ width: 100, height: 100, margin: 10 }}
                        >

                        </Image>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column'
                        }}>
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                    padding: 10
                                }}
                            >{this.props.item.name}</Text>
                            <Text style={{
                                fontSize: 14,
                                padding: 10,
                            }}>
                                {this.props.item.foodDescription}
                            </Text>
                        </View>
                    </View>

                    {/* Add seperated line */}
                    <View style={{
                        height: 1,
                        backgroundColor: 'white'
                    }}>

                    </View>
                </View>
            </Swipeout>


        )
    }
}

export default class DeleteItemFlatListExample extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            deletedRowKey: null,
        })
    }
    //use for refresh flat list item when deleted item.
    refreshFlatList = (deleteKey) => {
        this.setState(
            (prevState) => {
                return {
                    deletedRowKey: deleteKey
                }
            }
        )
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#ffffff',
                paddingTop: 40
            }}>
                <FlatList
                    data={flatListData}
                    renderItem={
                        ({ item, index }) => {
                            //console.log(`Item: ${JSON.stringify(item)} - Index: ${index}`)
                            return (
                                <FlatListItem item={item} index={index} parentFlatList={this}>

                                </FlatListItem>
                            )
                        }
                    }
                >
                </FlatList>
            </View>
        )
    }
}