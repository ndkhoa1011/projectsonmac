import React, {Component} from 'react'
import {Text, View, StyleSheet, Platform, TextInput, Dimensions, Button} from 'react-native'
import flatListData from '../data/flatListData'
import Modal from 'react-native-modalbox'

export default class EditModal extends Component {
    constructor(props){
        super(props)
        this.state={
            foodKey: '',
            foodName: '',
            foodDesc: ''
        }
    }

    _generateKey(numOfCharacters) {
        return require('random-string')(
            {length: numOfCharacters}
        )
    }

    _showModal(editingFood) {
        
        this.setState({
            foodKey: editingFood.key,
            foodName: editingFood.name,
            foodDesc: editingFood.foodDescription
        })

        this.refs.myModal.open()
    }

    render(){
        const screen = Dimensions.get('window');

        return(
            <Modal
                ref="myModal"
                style={{
                    justifyContent: 'center',
                    borderRadius: Platform.OS === "ios" ? 20 : 10,
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: 300
                 }}
                 position='center'
                 backdrop={true}
                 onClose={
                     () => {
                         //alert("OK")
                     }
                 }
            >
                <View style={{
                    flex: 1
                }}>
                    <View style={{
                        flex: 2,
                        //backgroundColor:'red',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            textAlign: 'center',
                        }}>
                            Edit Food's Information
                        </Text>
                    </View>
                    <View style={{
                        flex: 8,
                        //backgroundColor:'yellow'
                    }}>
                        <TextInput
                            style={styles.styleInput}
                            placeholder="Enter food name"
                            onChangeText={
                                (text) =>{
                                    this.setState({
                                        foodName: text
                                    })
                                }
                            }
                            autoFocus={true}
                            value={this.state.foodName}
                        >

                        </TextInput>

                        <TextInput
                            style={styles.styleInput}
                            placeholder="Enter food description"
                            onChangeText={
                                (text) => {
                                    this.setState({
                                        foodDesc: text
                                    })
                                }
                            }
                            value={this.state.foodDesc}
                        >

                        </TextInput>
                        <Button
                            
                            title="Save"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                            onPress={
                                () => {
                                    if(this.state.foodName.length == 0 ||
                                        this.state.foodDesc.length == 0) {
                                            alert("You must enter food name and description")
                                            return
                                        }
                                    //alert(this.state.foodKey)
                                    var index = flatListData.findIndex(t => this.state.foodKey == t.key)
                                    //alert(index)
                                    if(index < 0)
                                    {
                                        return;
                                    }

                                    flatListData[index].name = this.state.foodName
                                    flatListData[index].foodDescription = this.state.foodDesc
                                   
                                    this.props.parentFlatList._refreshFlatList(1)
                                    this.refs.myModal.close()
                                }
                            }
                        />

                    </View>
                </View>

            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    styleInput: {
        height: 40,
        borderBottomColor: 'gray',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 20,
        marginBottom: 10,
        borderBottomWidth: 1

    }
}
)
