import React, {Component} from 'react'
import {View, Text, TextInput, StyleSheet, ScrollView,Image, Dimensions} from 'react-native'

export default class VScrollViewExample extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputedText: ''
        }
    }
    render(){
        let screenWidth = Dimensions.get('window').width;

        return(
            <ScrollView
                maximumZoomScale={3}
                minimumZoomScale={1}
                keyboardDismissMode='on-drag'
            >
                <Image
                    source = {require('../images/image1.jpg')}
                    style={{
                        marginTop: 80,
                        width: screenWidth,
                        height: screenWidth * 491/655
                    }}
                >
                </Image>
                <Text style={{
                    padding: 15,
                    margin: 10,
                    fontSize: 20,
                    color: 'white',
                    backgroundColor: 'green',
                    textAlign:'center',                    
                }}>
                    This is a text
                </Text>

                <TextInput
                    style={{
                        padding: 15,
                        margin: 10,
                        borderWidth: 2,
                        borderRadius: 5,
                        borderColor: 'purple'
                    }}
                    placeholder="Please input your text"
                    placeholderTextColor='orange'
                    onChangeText={
                        (text) => {
                            this.setState(
                                (previousText) => {
                                    return{
                                        inputedText: text
                                    }
                                }
                            )
                        }
                    }
                >

                </TextInput>

                <Text style={{
                    padding: 15,
                    margin: 10,
                    fontSize: 20,
                    color: 'purple',
                }}>
                        {this.state.inputedText}
                </Text>

                <Image
                    source = {require('../images/image1.jpg')}
                    style={{
                        marginTop: 80,
                        width: screenWidth,
                        height: screenWidth * 491/655
                    }}
                >
                </Image>

                <Image
                    source = {require('../images/image1.jpg')}
                    style={{
                        marginTop: 80,
                        width: screenWidth,
                        height: screenWidth * 491/655
                    }}
                >
                </Image>

                <Image
                    source = {require('../images/image1.jpg')}
                    style={{
                        marginTop: 80,
                        width: screenWidth,
                        height: screenWidth * 491/655
                    }}
                >
                </Image>

                <Image
                    source = {require('../images/image1.jpg')}
                    style={{
                        marginTop: 80,
                        width: screenWidth,
                        height: screenWidth * 491/655
                    }}
                >
                </Image>
            </ScrollView>
        )
    }
}