import React, {Component} from 'react'
import {Text, View, StyleSheet, FlatList, Image, Alert, Platform, TextInput,
        TouchableHighlight, Dimensions, Button} from 'react-native'
import flatListData from '../data/flatListData'
import Modal from 'react-native-modalbox'

export default class AddModal extends Component {
    constructor(props){
        super(props)
        this.state={
            newFoodName: '',
            newFoodDesc: ''
        }
    }

    _generateKey(numOfCharacters) {
        return require('random-string')(
            {length: numOfCharacters}
        )
    }

    _showModal() {
        this.refs.myModal.open()
    }

    render(){
        const screen = Dimensions.get('window');

        return(
            <Modal
                ref="myModal"
                style={{
                    justifyContent: 'center',
                    borderRadius: Platform.OS === "ios" ? 20 : 10,
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: 300
                 }}
                 position='center'
                 backdrop={true}
                 onClose={
                     () => {
                         alert("OK")
                     }
                 }
            >
                <View style={{
                    flex: 1
                }}>
                    <View style={{
                        flex: 2,
                        //backgroundColor:'red',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            textAlign: 'center',
                        }}>
                            New Food's Information
                        </Text>
                    </View>
                    <View style={{
                        flex: 8,
                        //backgroundColor:'yellow'
                    }}>
                        <TextInput
                            style={styles.styleInput}
                            placeholder="Enter food name"
                            onChangeText={
                                (text) =>{
                                    this.setState({
                                        newFoodName: text
                                    })
                                }
                            }
                            autoFocus={true}
                        >

                        </TextInput>

                        <TextInput
                            style={styles.styleInput}
                            placeholder="Enter food description"
                            onChangeText={
                                (text) => {
                                    this.setState({
                                        newFoodDesc: text
                                    })
                                }
                            }
                        >

                        </TextInput>
                        <Button
                            
                            title="Save"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                            onPress={
                                () => {
                                    if(this.state.newFoodName.length == 0 ||
                                        this.state.newFoodDesc.length == 0) {
                                            alert("You must enter food name and description")
                                            return
                                        }
                                    const newKey = this._generateKey(24)
                                    const newFood = {
                                        key: newKey,
                                        name: this.state.newFoodName,
                                        imageUrl:"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Breakfast_at_Tamahan_Ryokan%2C_Kyoto.jpg/320px-Breakfast_at_Tamahan_Ryokan%2C_Kyoto.jpg",
                                        foodDescription: this.state.newFoodDesc
                                    };
                                    flatListData.push(newFood)
                                    this.props.parentFlatList._refreshFlatList(newKey)
                                    this.refs.myModal.close()
                                }
                            }
                        />

                    </View>
                </View>

            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    styleInput: {
        height: 40,
        borderBottomColor: 'gray',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 20,
        marginBottom: 10,
        borderBottomWidth: 1

    }
}
)
