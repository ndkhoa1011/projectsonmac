var express = require('express');
var router = express.Router();
let Food = require('../models/FoodModel');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/*Dinh nghia 1 phuong thuc moi de client co the oi toi */
router.get('/list_all_foods', (request, response, next) => {
  response.end('Get Request => list_all_foods');
})

//Dinh nghia phuong thuc post de insert du lieu
router.post('/insert_new_food', (request, response, next) => {
  //response.end('POST Request => insert_new_food');
  const newFood = new Food({
      name: request.body.name,
      foodDescription: request.body.foodDescription
  })

  newFood.save((err)=>{
      if(err) {
        response.json({
            result: "failed",
            data:{},
            message: `Error is: ${err}`
        });
      }
      else{
        response.json({
            result: "Ok",
            data:{
                name: request.body.name,
                foodDescription:  request.body.foodDescription,
                message: "Insert new food successfully"
            }
        })
      }
  })
})

//Dinh nghia phuong thuc PUT de update du lieu
router.put('/update_a_food', (request, response, next) => {
  response.end('PUT Request => update_a_food');
})

//Dinh nghia phuong thuc delete de xoa du lieu
router.delete('/delete_a_food', (request, response, next) => {
  response.end('DELETE Request => delete_a_food');
})