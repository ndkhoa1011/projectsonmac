/**HTTP MODULE */

module.exports={


    createServe: () => {
        var fileMgr = require('../src/fileManager')
        var fileName = __dirname + '/../data/temp.txt'

        let http = require('http');
        const port = 3001
        const server = http.createServer(
            (request, response) => {
                //response.write('this is a response from the NodeJs server.')
                response.writeHead(200, {
                    'Content-type': 'text/html'
                })
                const ipAddress = request.socket.remoteAddress;
                response.write(`Your IP address is: ${ipAddress} \r\n`)
                response.write(`Request url is: ${request.url} \r\n`)
                response.write(`Detail's url: ${JSON.stringify(require('url').parse(request.url, true).query)}`)
                debugger;
                
                response.end();
            }
        ).listen(port)

        console.log(`NodeJs server running on port ${port}`)
        console.log(`Test nodemon plugin running.`)
    }
}

