/**
 * Tat ca nhung phuong thuc xu li file se duoc viet trong fileManager.js
 */

 const fs = require('fs');

 module.exports={
     createNewFile: (fileName) => {
         const fd = fs.openSync(fileName, 'w');
     },
     saveJsonObjectToFile: (obj, fileName)=>{
         const jsonString = JSON.stringify(obj);

         fs.writeFile(fileName, jsonString, 'utf-8', (err, data) => {
            if(err) throw err;

            console.log(`Saved to file: ${fileName}`)
         })
     },
     readJsonObjectFromFile: (fileName) => {
         fs.readFile(fileName, (err, data) => {
             if(err) throw err;
             let jsonObj = JSON.parse(data);
             console.log(`jsonObject.foods = ${JSON.stringify(jsonObj)}`);
             console.log(`resultCode: ${JSON.stringify(jsonObj.resultCode)}`);
             console.log(`Restaurant Name; ${JSON.stringify(jsonObj.restaurantName)}`)
         })
     }
 }