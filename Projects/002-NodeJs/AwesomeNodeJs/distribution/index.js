// const rect = require('../AwesomeNodeJs/Shape/reactangle')

// console.log(`The area of the rectangle (width: 10, height: 20) is ${rect.area(10, 20)}`)

// console.log(`the circumference of the rectangle (width: 10, height: 20) is ${rect.circumference(10, 20)}`)

/**HTTP MODULE */

var http = require('http');
var port = 3001;
var server = http.createServer(function (request, response) {
    //response.write('this is a response from the NodeJs server.')
    response.writeHead(200, {
        'Content-type': 'text/html'
    });
    var ipAddress = request.socket.remoteAddress;
    response.write('Your IP address is: ' + ipAddress + ' \r\n');
    response.write('Request url is: ' + request.url + ' \r\n');
    response.write('Detail\'s url: ' + JSON.stringify(require('url').parse(request.url, true).query));
    debugger;
    response.end();
}).listen(port);

console.log('NodeJs server running on port ' + port);

console.log('Test nodemon plugin running.');